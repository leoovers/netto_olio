<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Nettopalkka</title>
</head>

<body>
    <div class="container">
        <h3>Nettopalkka</h3>
        <form action="laske.php" method="post">
            <div class="form-group">
                <label>Bruttopalkka</label>
                <input name="brutto" type="number" class="form-control">
            </div>
            <div>
                <label>Ennakkopidätys (%)</label>
                <input name="ennakko" type="number" class="form-control">
            </div>
            <div class="form-group">
                <label>Työeläke (%)</label>
                <input name="tyoelake" type="number" class="form-control">
            </div>
            <div class="form-group">
                <label>Työttömyysvakuutus (%)</label>
                <input name="tyottomyysvakuutus" type="number" class="form-control">
            </div>
            <button>Laske</button>
        </form>
    </div>

</body>

</html>