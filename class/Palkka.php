<?php
class Palkka {
    private $brutto = 0;
    private $netto = 0;
    private $ennakkopidatys = 0;
    private $ennakkopidatyssumma = 0;
    private $tyottomysvakuutus = 0;
    private $tyottomyysvakuutussumma = 0;
    private $tyoelake = 0;
    private $tyoelakesumma = 0;

    public function setBrutto($arvo) {
        $this->brutto=$arvo;
    }

    public function setEnnakkopidatys($arvo) {
        if ($ennakkopidatys>50) {
            throw new Exception ('Ennakkopidatysprosentti ei voi olla yli 50');
        }
        $this->ennakkopidatys = $arvo;
    }

    public function setTyottomyysvakuutus($arvo) {
        $this->tyottomysvakuutus = $arvo;
    }

    public function setTyoelake($arvo) {
        $this->tyoelake = $arvo;
    }

    public function getNetto() {
        return $this->netto;
    }

    public function getEnnakkopidatyssumma() {
        return $this->ennakkopidatyssumma;
    }

    public function getTyottomyysvakuutussumma() {
        return $this->tyottomyysvakuutussumma;
    }

    public function getTyoelakesumma() {
        return $this->tyoelakesumma;
    }

    public function laske() {
        $this->ennakkopidatyssumma = $this->brutto / 100 * $this->ennakkopidatys;
        $this->tyottomyysvakuutussumma = $this->brutto / 100 * $this->tyottomysvakuutus;
        $this->tyoelakesumma = $this->brutto / 100 * $this->tyoelake;
        $this->netto = $this->brutto - $this->ennakkopidatyssumma - $this->tyottomyysvakuutussumma - $this->tyoelakesumma;
    }
}
?>