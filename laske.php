<?php
require_once 'class/Palkka.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Nettopalkka</title>
</head>
<body>
<div class="container">
<h3>Nettopalkka</h3>
<?php
$palkka = new Palkka();
$palkka->setBrutto(filter_input(INPUT_POST, 'brutto',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));
$palkka->setEnnakkopidatys(filter_input(INPUT_POST, 'ennakko',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));
$palkka->setTyottomyysvakuutus(filter_input(INPUT_POST, 'tyoelake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));
$palkka->setTyoelake(filter_input(INPUT_POST, 'tyottomyysvakuutus',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));
$palkka->laske();
print '<p>Ennakkopidätys: ' . $palkka->getEnnakkopidatyssumma() . '</p>';
print '<p>Työeläkemaksu: ' . $palkka->getTyoelakesumma() . '</p>';
print '<p>Työttömyysvakuutusmaksu: ' . $palkka->getTyottomyysvakuutussumma() . '</p>';
print '<p>Nettopalkka: ' . $palkka->getNetto() . '</p>';
?>
<a href="index.php">Laske uudestaan</a>
</div>    
</body>
</html>